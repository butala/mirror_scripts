#!/usr/bin/env python

import os
import logging
import cPickle
from ftplib import FTP
from datetime import datetime

logger = logging.getLogger('mirror_omni')


TIMEOUT = 10  # in seconds


LOCAL_ROOT = '/rdata/airglow/butala/data/omni'


INDEX_PKL = os.path.join(LOCAL_ROOT, 'index.pkl')



def get_asc_mtime_map(ftp):
    ls = []
    ftp.retrlines('MLSD', ls.append)
    mtime_map = {}
    for ls_i in ls:
        toks = ls_i.split(';')
        modify_tok = toks[0]
        assert modify_tok.startswith('modify=')
        modify = int(modify_tok.split('=')[-1])
        asc_fname = toks[-1].strip()
        if not asc_fname.endswith('.asc'):
            continue
        mtime_map[asc_fname] = modify
    return mtime_map


def get_mirror_mtime_map(index_pkl=INDEX_PKL):
    if os.path.isfile(index_pkl):
        with open(index_pkl) as fid:
            mirror_mtime_map = cPickle.load(fid)
    else:
        logger.info('{} does not exist, starting with empty record'.format(index_pkl))
        mirror_mtime_map = {}
    return mirror_mtime_map


def save_mirror_mtime_map(mirror_mtime_map,
                          index_pkl=INDEX_PKL):
    with open(index_pkl, 'w') as fid:
        cPickle.dump(mirror_mtime_map, fid)
    return index_pkl


def get_fetch_list(ftp, mirror_mtime_map):
    mtime_map = get_asc_mtime_map(ftp)
    fetch_list = []
    for fname in sorted(mtime_map):
        if (fname not in mirror_mtime_map) or \
           (mirror_mtime_map[fname] != mtime_map[fname]):
            fetch_list.append((fname, mtime_map[fname]))
    return fetch_list


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    mirror_mtime_map = get_mirror_mtime_map()

    try:
        ftp = FTP('spdf.gsfc.nasa.gov',
                  '',
                  '',
                  '',
                  TIMEOUT)
        ftp.login()
        ftp.cwd('/pub/data/omni/high_res_omni/monthly_1min')

        fetch_list = get_fetch_list(ftp, mirror_mtime_map)

        for fname, mtime in fetch_list:
            local_fname = os.path.join(LOCAL_ROOT, fname)
            try:
                with open(local_fname, 'w') as fid_out:
                    logger.info('{} -> {}'.format(fname,
                                                  local_fname))
                    ftp.retrbinary('RETR {}'.format(fname),
                                   fid_out.write)
                assert os.path.isfile(local_fname)
                mirror_mtime_map[fname] = mtime
            except:
                if os.path.isfile(local_fname):
                    # prevent partial downloads on error
                    os.remove(local_fname)
                raise
    finally:
        ftp.quit()
        save_mirror_mtime_map(mirror_mtime_map)
