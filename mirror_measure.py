#!/usr/bin/env python

import os
import logging
from urllib2 import urlopen
from contextlib import closing


ROOT_URL = 'http://vmo.igpp.ucla.edu/data1/MEASURE/ASCII/'


LOCAL_ROOT = '/rdata/airglow/butala/data/measure'


SITES = ['APL',
         'CLK',
         'DSO',
         'FIT',
         'GTF',
         'JAX',
         'MSH',
         'USC']


LOGGER = logging.getLogger('mirror_measure')


def get_years(site):
    years = []
    site_url = ROOT_URL + site
    with closing(urlopen(site_url)) as fid:
        for line in fid:
            if line.startswith('<a href=') and site in line:
                toks = line.split('<tt>')
                years.append(toks[1][:4])
    return years


def get_fetch_list(site, year):
    fetch_list = []
    year_url = ROOT_URL + site + '/' + year
    with closing(urlopen(year_url)) as fid:
        for line in fid:
            if line.startswith('<a href=') and site + '.asc' in line:
                toks = line.split('<tt>')
                fname = toks[1].split('</tt>')[0]
                fetch_list.append(year_url + '/' + fname)
    return fetch_list


def fetch_data(fetch_list, site, year):
    local_fnames = []
    for url in fetch_list:
        basename = url.split('/')[-1]
        local_path = os.path.join(LOCAL_ROOT, year, site)
        if not os.path.isdir(local_path):
            LOGGER.info('creating {}'.format(local_path))
            os.makedirs(local_path)
        local_fname = os.path.join(local_path, basename)
        if os.path.isfile(local_fname):
            LOGGER.info('{} exists --- skipping'.format(local_fname))
            continue
        LOGGER.info('{} -> {}'.format(url, local_fname))
        with open(local_fname, 'w') as local_fid, closing(urlopen(url)) as remote_fid:
            local_fid.write(remote_fid.read())
        assert os.path.isfile(local_fname)
        local_fnames.append(local_fname)
    return local_fnames


def mirror_measure(sites=SITES):
    for site in sites:
        for year in get_years(site):
            fetch_list = get_fetch_list(site, year)
            fetch_data(fetch_list, site, year)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    mirror_measure()
