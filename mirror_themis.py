#!/usr/bin/env python

import logging
import os
import cPickle
from urllib2 import urlopen
from contextlib import closing
from collections import defaultdict

LOGGER = logging.getLogger('mirror_intermagnet_sec')

URL_ROOT = 'http://themis.ssl.berkeley.edu/data/themis/thg/ascii_data/mag/'

ROOT = '/rdata/airglow/butala/data/themis'


SITES = [
    # FALCON
    'hris',
    'stfd',
    'pblo',
    'lrel',
    'wlps',
    # McMAC
    'glyn',
    'wrth',
    'benn',
    'amer',
    'pcel',
    'rich',
    'satx',
    'lyfd',
    # USGS
    'new',
    'frn',
    'tuc',
    'bou',
    'bsl',
    'frd',
    # THEMIS EPO
    'hots',
    'ukia',
    'ccnv',
    'fyts',
    'pine',
    'swno',
    'bmls',
    'rmus',
    'loys',
    'drby']


# SITES = ['hris', 'bsl']

# SITES = ['bsl']


def get_years(url):
    years = []
    with closing(urlopen(url)) as root:
        for line in root:
            toks = line.split('<a href="')
            if len(toks) > 1 and toks[1][:4].isdigit():
                year_str = toks[1][:4]
                years.append(year_str)
    return years


def is_flat(url):
    with closing(urlopen(url)) as root:
        for line in root:
            if 'folder.gif' in line:
                return False
    return True


def get_flat(url, site):
    fetch_list = []
    with closing(urlopen(url)) as root:
        for line in root:
            toks = line.split('<a href="')
            if len(toks) >= 2 and toks[1].startswith('thg_l2_mag'):
                fname = toks[1].split('"')[0]
                fetch_list.append(url + fname)
    return fetch_list


def get_months(url):
    months = []
    with closing(urlopen(url)) as root:
        for line in root:
            toks = line.split('<a href="')
            if len(toks) >=2 and toks[1][:2].isdigit():
                months.append(toks[1][:2])
    return months


def get_month(url, site):
    fetch_list = []
    for month in get_months(url):
        # http://themis.ssl.berkeley.edu/data/themis/thg/ascii_data/mag/bsl/2007/01/
        month_url = url + month + '/'
        with closing(urlopen(month_url)) as root:
            for line in root:
                toks = line.split('<a href="')
                if len(toks) >= 2 and toks[1].startswith('thg_l2_mag'):
                    fname = toks[1].split('"')[0]
                    fetch_list.append(month_url + fname)
    return fetch_list


def fetch_urls(fetch_list, year, site, root=ROOT):
    local_fnames = []
    local_path = os.path.join(root, year, site)
    if not os.path.isdir(local_path):
        LOGGER.info('creating path {}'.format(local_path))
        os.makedirs(local_path)
    for fetch_url in fetch_list:
        fname = fetch_url.split('/')[-1]
        local_fname = os.path.join(local_path, fname)
        if os.path.isfile(local_fname):
            LOGGER.info('{} exists --- skipping'.format(local_fname))
            continue
        LOGGER.info('{} -> {}'.format(fetch_url, local_fname))
        with open(local_fname, 'w') as local_fid, closing(urlopen(fetch_url)) as fetch_fid:
            local_fid.write(fetch_fid.read())
        assert os.path.isfile(local_fname)
        local_fnames.append(local_fname)
    return local_fnames


def get_year_data(year, site):
    url = URL_ROOT + site + '/' + year + '/'
    if is_flat(url):
        fetch_list = get_flat(url, site)
    else:
        fetch_list = get_month(url, site)
    return fetch_urls(fetch_list, year, site)


def mirror_themis(sites=SITES):
    local_fnames = []
    for site in sites:
        LOGGER.info('mirroring {}'.format(site))
        for year in get_years(URL_ROOT + site):
            local_fnames.extend(get_year_data(year, site))
    return local_fnames


def index_year_data(year, site):
    url = URL_ROOT + site + '/' + year + '/'
    if is_flat(url):
        fetch_list = get_flat(url, site)
    else:
        fetch_list = get_month(url, site)
    return fetch_list


def index_themis(sites=SITES):
    index = defaultdict(dict)
    for site in sites:
        LOGGER.info('indexing {}'.format(site))
        for year in get_years(URL_ROOT + site):
            index[int(year)][site] = index_year_data(year, site)
        with open('/rdata/airglow/butala/data/themis/index.pkl', 'w') as fid:
            cPickle.dump(index, fid, -1)
    return index


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    index_themis()
    # mirror_themis()
