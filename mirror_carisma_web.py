#!/usr/bin/env python

import os
import sys
import json
import time
import logging
import cPickle
from zipfile import ZipFile
from contextlib import closing
from datetime import datetime
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

import pandas as PD
from splinter import Browser

LOGGER = logging.getLogger('mirror_carisma_web')


def parse_name(name):
    dt = datetime.strptime(name[:8], '%Y%m%d')
    site = name[8:12].lower()
    return dt, site


def get_processed_dates(index_pkl):
    try:
        with open(index_pkl) as fid:
            return cPickle.load(fid)
    except IOError:
        LOGGER.warning('{} does not exist --- starting from empty set of processed dates'.format(index_pkl))
        return set()


def save_processed_dates(index_pkl, processed_dates):
    with open(index_pkl, 'w') as fid:
        cPickle.dump(processed_dates, fid)
    return index_pkl


def process_date(local_root,
                 download_path,
                 browser,
                 year,
                 month,
                 select_site=True,
                 N_attemptes=100):
    start_date = browser.find_by_id('StartDate').first
    start_date.value = '{}-{:02d}-01'.format(year, month)

    if select_site:
        number_of_days = browser.find_by_name('no_days').first
        # set to 31 number of days (last option)
        number_of_days.find_by_tag('option')[-1].click()

    if select_site:
        # select sites
        browser.choose('sites_req', 'userdef')
        time.sleep(0.5)
        browser.select('user_sites[]', 'ANNA')
        browser.select('user_sites[]', 'POLS')
        browser.select('user_sites[]', 'OSAK')
        browser.select('user_sites[]', 'THRF')

    time.sleep(0.2)
    browser.find_by_value('Request Data...').first.click()

    n = 0
    while True:
        if len(browser.find_by_value('Download file')) > 0:
            break
        n += 1
        if n > N_attempts:
            raise RuntimeError('number of attempts exceeded')
        time.sleep(0.2)

    time.sleep(0.2)

    download = browser.find_by_value('Download file').first
    download.click()

    n = 0
    while True:
        time.sleep(0.5)
        zip_fnames = filter(lambda x: 'fgm' in x and 'zip' in x,
                           os.listdir(download_path))
        # print(zip_fnames)
        if len(zip_fnames) == 1 and os.stat(os.path.join(download_path, zip_fnames[0])).st_size > 1000:
            break
        n += 1
        if n > N_attempts:
            raise RuntimeError('number of attempts exceeded')

    zip_fname = os.path.join(download_path, zip_fnames[0])
    with closing(ZipFile(zip_fname)) as zip_fid:
        for name in zip_fid.namelist():
            dt, site = parse_name(name)
            local_path = os.path.join(local_root, str(dt.year), site)
            if not os.path.isdir(local_path):
                LOGGER.info('creating {}'.format(local_path))
                os.makedirs(local_path)
            zip_fid.extract(name, local_path)
    os.remove(zip_fname)


def mirror_carisma_web(config):
    processed_dates = get_processed_dates(config['index_pkl'])
    first_iteration = True
    prof = {}
    prof['browser.download.manager.showWhenStarting'] = 'false'
    prof['browser.helperApps.alwaysAsk.force'] = 'false'
    prof['browser.download.dir'] = config['download_path']
    prof['browser.download.folderList'] = 2
    prof['browser.helperApps.neverAsk.saveToDisk'] = 'applitcation/zip, application/octet-stream'
    prof['browser.download.manager.useWindow'] = 'false'
    prof['browser.helperApps.useWindow'] = 'false'
    prof['browser.helperApps.showAlertonComplete'] = 'false'
    prof['browser.helperApps.alertOnEXEOpen'] = 'false'
    prof['browser.download.manager.focusWhenStarting']= 'false'

    with Browser('firefox',
                 profile=config['profile'],
                 profile_preferences=prof) as browser:
        data_url = 'https://www.carisma.ca/carisma-data/carisma-data-repository'
        for year in range(config['start_year'],
                          config['stop_year'] + 1):
            for month in range(1, 13):
                date = datetime(year, month, 1)
                if date < config['min_date']:
                    continue
                if date > config['max_date']:
                    continue
                date_pair = (year, month)
                if date_pair in processed_dates:
                    LOGGER.info('{}-{:02d} already processed --- skipping'.format(*date_pair))
                    continue
                LOGGER.info('fetching {}-{:02d}'.format(*date_pair))
                browser.visit(data_url)
                if first_iteration:
                    browser.fill('username', config['username'])
                    browser.fill('password', config['password'])
                    browser.find_by_text('Log in').first.click()
                process_date(browser, year, month, select_site=first_iteration)
                processed_dates.add(date_pair)
                save_processed_dates(config['index_pkl'], processed_dates)
                first_iteration = False
    return processed_dates


def parse_config(config_fname):
    with open(config_fname) as fid:
        config = json.load(fid)
        try:
            config = config['carisma_web']
        except KeyError:
            raise KeyError('could not find "carisma_web" section in configuration file {}'.format(config_fname))
    config['min_date'] = datetime(*config['min_date'])
    config['max_date'] = datetime(*config['max_date'])
    config['index_pkl'] = os.path.join(config['local_root'],
                                       config['index_fname'])
    assert os.path.isdir(config['local_root'])
    assert os.path.isfile(config['index_pkl'])
    return config


def main(argv=None):
    if argv is None:
        argv = sys.argv

    parser = ArgumentParser('Mirror CARISMA data via scripted web page interaction.',
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('config_json',
                        type=str,
                        help='JSON configuration file')
    args = parser.parse_args(argv[1:])

    config = parse_config(args.config_json)

    mirror_carisma_web(config)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    sys.exit(main())
