#!/usr/bin/env python

import os
import logging
import posixpath
from datetime import datetime
from urllib2 import urlopen
from contextlib import closing

import sh

logger = logging.getLogger('mirror_dscovr')


LOCAL_ROOT = '/rdata/airglow/butala/data/DSCOVR'
"""Root path to store local mirror."""


URL_ROOT = 'https://www.ngdc.noaa.gov/dscovr/data/'
""" ??? """


def remote_url_list(url_root=URL_ROOT,
                    start_year=2015,
                    end_year=datetime.now().year):
    url_list = []
    for year in range(start_year, end_year + 1):
        for month in range(1, 13):
            logger.info('processing {}-{:02d}'.format(year, month))
            url = posixpath.join(url_root, str(year), '{:02d}'.format(month))
            try:
                with closing(urlopen(url)) as fid:
                    for line in fid:
                        if '.nc.gz' in line:
                            fname = line.split()[5].split('"')[1]
                            url_list.append((posixpath.join(url, fname), year, month))
            except:
                logger.warning('could not open {} --- skipping'.format(url))
                continue
    return url_list


def select_urls(url_list):
    out_list = []
    for url, year, month in url_list:
        fname = posixpath.basename(url)
        if any([fname.startswith(x) for x in ['oe_f1m', 'oe_f3s', 'oe_m1m', 'oe_m1s']]):
            out_list.append((url, year, month))
    return out_list


def mirror_urls(local_root, url_list):
    local_fnames = []
    for url, year, month in url_list:
        local_gz_fname = os.path.join(local_root,
                                      str(year),
                                      '{:02d}'.format(month),
                                      posixpath.basename(url))
        assert local_gz_fname.endswith('.gz')
        local_fname = local_gz_fname[:-3]
        if not os.path.isfile(local_fname):
            logger.info('{} does not exist --- fetching'.format(local_fname))
            local_path = os.path.dirname(local_gz_fname)
            if not os.path.isdir(local_path):
                logger.info('{} does not exist --- creating path'.format(local_path))
                os.makedirs(local_path)
            with open(local_gz_fname, 'w') as out_fid, closing(urlopen(url)) as in_fid:
                out_fid.write(in_fid.read())
            sh.gzip(['-d', local_gz_fname], _cwd=local_path)
            assert os.path.isfile(local_fname)
            local_fnames.append(local_fname)
    return local_fnames


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    url_list = remote_url_list()
    filtered_urls = select_urls(url_list)
    mirror_urls(LOCAL_ROOT,
                filtered_urls)
