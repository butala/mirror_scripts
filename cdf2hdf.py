import sys
import logging
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from collections import defaultdict
from datetime import datetime

import pandas as PD
import sh


CDF_DUMP = '/home/butala/src/cdf36/cdf36_3-dist/src/tools/cdfdump'
"""
Path to CDF dump program (built from code at
https://cdf.gsfc.nasa.gov/).
"""


def cdf2hdf(hdf_fname,
            cdf_fname,
            parse_map,
            key,
            cdf_dump=CDF_DUMP):
    """
    Read the contents of the CDF file *cdf_fname* and output to the
    HDF5 format file *hdf_fname*. Process variables specified by
    *parse_map*, a mapping between variable names and a function that
    return a mapping between keys and parsed values. Associate the
    result under *key*. Use the CDF dump program *cdf_dump*.
    """
    # parse CDF file
    cmd = sh.Command(cdf_dump)
    args = ['-dump', 'data']
    args.append('-vars')
    args.append(','.join(parse_map.keys()))
    args.append(cdf_fname)
    data_map = defaultdict(list)
    for line in cmd(args, _iter=True):
        if 'No: ' in line and '(Recs: ' in line:
            cols = line.split()
            var_name = cols[0]
            N = int(cols[-1][:-1])
            pad_value = None
        elif line.lstrip().startswith('Pad value:'):
            pad_value = line.split(':', 1)[1].strip()
        elif line.lstrip().startswith('Record #'):
            _, data = line.split(':', 1)
            for k, v in parse_map[var_name](data, pad_value=pad_value).iteritems():
                data_map[k].append(v)
    # store data to HDF file
    dt = data_map.pop('dt')
    df = PD.DataFrame(index=dt,
                      data=data_map)
    df.to_hdf(hdf_fname, key)
    return hdf_fname


def parse_dt(x, pad_value=None):
    """
    Parse a CDF date/time data line *x*. The *pad_value* is ignored.
    """
    return {'dt': datetime.strptime(x.strip(), '%d-%b-%Y %H:%M:%S.%f')}


def parse_3vec(key):
    """
    Return a 3 element CDF vector parser. Associate the result with
    column identifier *key*, i.e., the output map will have 3 entries:
    *key*_x, *key*_y, and *key*_z. Empty lines (all entries equal to
    *pad_value*) will return a 3 vector of nans.
    """
    def parse_3vec_inner(x, pad_value=None):
        x = x.strip()
        assert x[0] == '[' and x[-1] == ']'
        elements = x[1:-1].split(',')
        assert len(elements) == 3
        if elements[0] == pad_value:
            assert len(set(elements)) == 1
            elements = ['nan'] * 3
        return {key + '_{}'.format(component): float(v) for component, v in zip(['x', 'y', 'z'],
                                                                                elements)}
    return parse_3vec_inner


def parse_float(key):
    """
    Return a CDF float parser. Associate the result with
    *key*. Encountered pad values will be converted to nan.
    """
    def parse_float_inner(x, pad_value=None):
        x = x.strip()
        if x == pad_value:
            x = 'nan'
        return {key: float(x)}
    return parse_float_inner


def swe2hdf(hdf_fname, cdf_fname):
    """
    Convert SWE data file *cdf_fname* to HDF format *hdf_fname*.
    """
    parse_map = {'Epoch': parse_dt,
                 'V_GSM': parse_3vec('V_GSM'),
                 'Np': parse_float('Np')}
    return cdf2hdf(hdf_fname,
                   cdf_fname,
                   parse_map,
                   'swe')


def mfi2hdf(hdf_fname, cdf_fname):
    """
    Convert MFI data file *cdf_fname* to HDF format *hdf_fname*.
    """
    parse_map = {'Epoch': parse_dt,
                 'BGSM': parse_3vec('BGSM')}
    return cdf2hdf(hdf_fname,
                   cdf_fname,
                   parse_map,
                   'mfi')
