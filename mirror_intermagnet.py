#!/usr/bin/env python

import logging
import re
import os
import sys
import json
from ftplib import FTP
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

import sh


LOGGER = logging.getLogger('mirror_intermagnet')


YYYY_RE = re.compile('\d{4}')


def uncompress(fname):
    LOGGER.info('uncompressing ' + fname)
    base_fname, ext = os.path.splitext(fname)
    basename = os.path.basename(fname)
    dirname = os.path.dirname(fname)
    if ext == '.zip':
        sh.unzip('-j', basename,
                 _cwd=dirname)
        os.remove(fname)
    elif ext == '.gz':
        sh.gunzip(basename,
                  _cwd=dirname)
    elif ext == '.min':
        # no uncompression necessary
        return fname
    else:
        raise RuntimeError('cannot uncompress {} (ext={})'.format(fname, ext))
    return base_fname


def get_definitive(ftp, local_root, start_year=1991):
    LOGGER.info('mirroring /definitive')
    ftp.cwd('/definitive')
    # find YYYY sub directories
    yyyy_subdirs = []
    def get_year_paths(s):
        for line in s.splitlines():
            if line[0] == 'd':
                cols = line.split()
                if YYYY_RE.match(cols[-1]):
                    yyyy_subdirs.append(cols[-1])
    ftp.dir(get_year_paths)
    # mirror each year
    for yyyy in yyyy_subdirs:
        if int(yyyy) < start_year:
            continue
        LOGGER.info('processing ' + yyyy)
        ftp.cwd('/definitive/' + yyyy)
        # mirror each station
        for stn in ftp.nlst():
            if len(stn) != 3:
                continue
            LOGGER.info('processing ' + stn)
            try:
                ftp.cwd('/definitive/' + yyyy + '/' + stn + '/IAGA2002')
            except:
                LOGGER.error('could not remote cd to {} --- skipping'.format('/definitive/' + yyyy + '/' + stn + '/IAGA2002'))
                continue
            for fname in ftp.nlst():
                if not fname.startswith(stn) or len(fname) <= len(stn) or len(fname) <= 11:
                    LOGGER.warning('{} not recognized as IAGA2002 zip file --- skipping'.format(fname))
                    continue
                base_fname, ext = os.path.splitext(fname)
                local_fname = os.path.join(local_root, yyyy, stn, 'IAGA2002', fname)
                uncompressed_local_fname = os.path.join(local_root, yyyy, stn, 'IAGA2002', base_fname)
                if os.path.isfile(uncompressed_local_fname):
                    LOGGER.debug('already downloaded {} --- skipping'.format(fname))
                    continue
                else:
                    LOGGER.info('storing {} to {}'.format(fname, local_fname))
                    dirname = os.path.dirname(local_fname)
                    if not os.path.isdir(dirname):
                        os.makedirs(dirname)
                    try:
                        with open(local_fname, 'w') as fid:
                            ftp.retrbinary('RETR {}'.format(fname),
                                           fid.write)
                        local_fname_after_uncompression = uncompress(local_fname)
                        assert os.path.isfile(local_fname_after_uncompression)
                    except Exception, e:
                        LOGGER.error(str(e))
                        if os.path.isfile(local_fname):
                            # prevent partial downloads on error
                            os.remove(local_fname)
                        remote_fullpath = '/definitive/' + yyyy + '/' + stn + '/IAGA2002/' + fname
                        LOGGER.info('could not download {}'.format(remote_fullpath))
                        continue
    return ftp


def mirror_intermagnet(config):
    try:
        ftp = FTP('ftp.intermagnet.org',
                  '',
                  '',
                  '',
                  config['timeout'])
        ftp.login(config['user'],
                  config['password'])

        get_definitive(ftp,
                       config['local_root'],
                       start_year=config['start_year'])
    finally:
        ftp.quit()


def parse_config(config_fname):
    with open(config_fname) as fid:
        config = json.load(fid)
        try:
            return config['intermagnet']
        except KeyError:
            raise KeyError('could not find "intermagnet" section in configuration file {}'.format(config_fname))


def main(argv=None):
    if argv is None:
        argv = sys.argv

    parser = ArgumentParser('Mirror 1-minute interval, definitive INTERMAGNET data.',
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('config_json',
                        type=str,
                        help='JSON configuration file')
    args = parser.parse_args(argv[1:])

    config = parse_config(args.config_json)

    while True:
        try:
            mirror_intermagnet(config)
            break
        except Exception, e:
            LOGGER.error(str(e))
            LOGGER.info('retrying mirror_intermagnet process')


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    logging.getLogger('sh').setLevel(logging.WARNING)
    sys.exit(main())
