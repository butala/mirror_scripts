#!/usr/bin/env python

import os
import sys
import logging
import posixpath
import shutil
from datetime import datetime
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from contextlib import closing
from ftplib import FTP
from collections import defaultdict

import sh
from dateutil.parser import parse

from pyrsss.gps.teqc import rinex_merge
from pyrsss.util.date import date2dt
from pyrsss.util.path import SmartTempDir, touch_path, replace_path

logger = logging.getLogger('mirror_txdot')

SERVER = 'ftp.dot.state.tx.us'

OUTPUT_PATH = '/rdata/airglow/butala/data/txdot'



def get_station_map(ftp, date):
    """
    ???
    """
    logger.info('getting station map')
    ftp.cwd('/pub/txdot-info/isd/gps/RINEX')
    stn_map = defaultdict(list)
    for stn in ftp.nlst():
        if stn.startswith('tx'):
            logger.info('listing {}'.format(stn))
            remote_path = '/pub/txdot-info/isd/gps/RINEX/' + stn
            ftp.cwd(remote_path)
            for fname in ftp.nlst():
                if fname.startswith('{stn}{date:%j}'.format(stn=stn,
                                                            date=date)) and \
                   fname.endswith('.{date:%y}o.zip'.format(date=date)):
                    stn_map[stn].append(posixpath.join(remote_path,
                                                       fname))
    return stn_map


def fetch_txdot(work_path,
                ftp,
                remote_rinex_list):
    """
    ???
    """
    local_fnames = []
    for rinex_fname in remote_rinex_list:
        local_fnames.append(os.path.join(work_path,
                                          posixpath.basename(rinex_fname)))
        logger.info('fetching {} and storing to {}'.format(rinex_fname,
                                                           local_fnames[-1]))
        with open(local_fnames[-1], 'w') as fid:
            ftp.retrbinary('RETR ' + rinex_fname,
                           fid.write)
    return local_fnames


def merge_rinex(output_prefix,
                rinex_list):
    """
    ???
    """
    _7za = sh.Command('7za')
    obs = []
    navs = []
    gnavs = []
    # fetch RINEX file records
    for rinex_fname in rinex_list:
        _7za('-y',
             'x',
             os.path.basename(rinex_fname),
             _cwd=os.path.dirname(rinex_fname),
             _out=sys.stdout,
             _err=sys.stderr)
        obs.append(rinex_fname[:-4])
        navs.append(obs[-1][:-1] + 'n')
        gnavs.append(obs[-1][:-1] + 'g')
        # make sure obs, nav, and gnav files were successfully unpacked
        assert all([os.path.isfile(x) for x in [obs[-1], navs[-1], gnavs[-1]]])
    # merge RINEX file records
    touch_path(os.path.dirname(output_prefix))
    rinex_fname = output_prefix + 'o'
    rinex_merge(rinex_fname, obs)
    nav_fname = output_prefix + 'n'
    rinex_merge(nav_fname, navs)
    gnav_fname = output_prefix + 'g'
    rinex_merge(gnav_fname, gnavs)
    return (rinex_fname,
            nav_fname,
            gnav_fname)


def get_output_prefix(path, stn, date):
    """
    ???
    """
    return os.path.join(path,
                        date.strftime('%Y'),
                        date.strftime('%j'),
                        '{stn}{date:%j}0.{date:%y}'.format(stn=stn,
                                                           date=date))


def mirror_txdot(output_path,
                 date,
                 server=SERVER,
                 work_path=None):
    """
    ???
    """
    logger.info('opening ftp connection to {}'.format(server))
    rinex_map = {}
    with closing(FTP(SERVER)) as ftp, \
         SmartTempDir(work_path) as work_path:
        ftp.login()
        # get names of TxDOT RINEX files
        logger.info('getting station RINEX map')
        stn_map = get_station_map(ftp, date)
        # fetch RINEX files
        fetch_map = {}
        for stn in sorted(stn_map):
            if len(stn_map[stn]) == 24:
                fetch_map[stn] = fetch_txdot(work_path,
                                             ftp,
                                             stn_map[stn])
            else:
                logger.warning('gap(s) detected in hourly RINEX file record '
                               'for {} on {:%Y-%m-%d} (only {} files '
                               'found)--- skipping'.format(stn,
                                                           date,
                                                           len(stn_map[stn])))
                continue
        # merged fetched RINEX files
        for stn in sorted(fetch_map):
            rinex_map[stn] = merge_rinex(get_output_prefix(output_path, stn, date),
                                         fetch_map[stn])
    return rinex_map


def main(argv=None):
    if argv is None:
        argv = sys.argv

    parser = ArgumentParser('Mirror TxDOT RINEX data.',
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('date',
                        type=parse,
                        nargs='?',
                        default=date2dt(datetime.utcnow().date()),
                        help='mirror data for the given date')
    parser.add_argument('--work-path',
                        '-w',
                        type=str,
                        default=None,
                        help='path to store intermediate files')
    parser.add_argument('--output-path',
                        '-o',
                        type=str,
                        default=OUTPUT_PATH,
                        help='path to store daily TxDOT RINEX files')
    args = parser.parse_args(argv[1:])

    mirror_txdot(args.output_path,
                 args.date,
                 work_path=args.work_path)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    logging.getLogger('sh').setLevel(logging.WARNING)
    sys.exit(main())
