#!/usr/bin/env python

import logging
import os
from ftplib import FTP

LOGGER = logging.getLogger('mirror_ACE')


TIMEOUT = 10  # in seconds

SUBDIRS = ['mag',
           'swepam',
           'multi',
           'uleis_v2',
           'epam',
           'sepica',
           'sis_v3',
           'cris',
           'ssv4']

LOCAL_ROOT = '/rdata/airglow/butala/data/ACE/level2'


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    try:
        ftp = FTP('mussel.srl.caltech.edu',
                  '',
                  '',
                  '',
                  TIMEOUT)
        ftp.login()
        for subdir in sorted(SUBDIRS):
            remote_path = '/pub/ace/level2/{}'.format(subdir)
            LOGGER.info('Porcessing {}'.format(remote_path))
            ftp.cwd(remote_path)
            local_path = os.path.join(LOCAL_ROOT, subdir)
            if not os.path.isdir(local_path):
                os.makedirs(local_path)
            for remote_fname in ftp.nlst():
                if remote_fname.rstrip().endswith('.hdf'):
                    local_fname = os.path.join(local_path, remote_fname.strip())
                    if not os.path.isfile(local_fname):
                        LOGGER.info('fetching {} and saving to {}'.format(remote_fname, local_fname))
                        try:
                            with open(local_fname, 'w') as fid:
                                ftp.retrbinary('RETR {}'.format(remote_fname),
                                               fid.write)
                        except:
                            if os.path.isfile(local_fname):
                                # prevent partial downloads on error
                                os.remove(local_fname)
    finally:
        ftp.quit()
