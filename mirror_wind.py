#!/usr/bin/env python

import os
import ftplib
import logging
import posixpath
from urllib2 import urlopen
from tempfile import NamedTemporaryFile
from contextlib import closing
from gzip import GzipFile
from datetime import datetime

from cdf2hdf import swe2hdf, mfi2hdf

logger = logging.getLogger('mirror_wind')


LOCAL_ROOT = '/rdata/airglow/butala/data/wind'
"""Root path to store local mirror."""

SWE_HOST = 'cdaweb.gsfc.nasa.gov'
"""FTP host for SWE data."""

SWE_ROOT = '/pub/data/wind/swe/swe_k0'
"""Root path to SWE data on FTP host."""


MFI_ROOT = 'https://wind.nasa.gov/mission/wind/mfi/'
"""Root URL to MFI files."""


def get_mfi_files(mfi_root=MFI_ROOT):
    fnames = []
    with closing(urlopen(mfi_root)) as fid:
        for line in fid:
            if 'wi_h0_mfi' in line:
                fnames.append(line.split('"')[1])
    return fnames


def mirror_mfi(root,
               url_root='https://wind.nasa.gov/mission/wind/mfi/'):
    logger.info('mirroring MFI')
    all_fnames = get_mfi_files()
    new_fnames = []
    for fname in all_fnames:
        local_fname = os.path.join(root, fname)
        if not os.path.isfile(local_fname):
            logger.info('fetching missing file {}'.format(local_fname))
            remote_fname = url_root + fname
            with open(local_fname, 'w') as out_fid, closing(urlopen(remote_fname)) as in_fid:
                out_fid.write(in_fid.read())
            new_fnames.append(local_fname)
        # convert to HDF
        local_hdf_fname = local_fname[:-3] + 'hdf'
        if not os.path.isfile(local_hdf_fname):
            logger.info('converting {} to {}'.format(local_fname,
                                                     local_hdf_fname))
            mfi2hdf(local_hdf_fname,
                    local_fname)
    return new_fnames


def mirror_swe(root,
               swe_host=SWE_HOST,
               swe_root=SWE_ROOT,
               start_year=1994,
               stop_year=datetime.now().year):
    logger.info('mirroring SWE')
    local_fnames = []
    new_fnames = []
    with closing(ftplib.FTP(swe_host)) as ftp:
        ftp.login()
        for year in range(start_year, stop_year):
            logger.info('processing {}'.format(year))
            path = posixpath.join(swe_root, str(year))
            try:
                ftp.cwd(path)
            except:
                logger.warning('could not access {} on {} --- skippping'.format(path, swe_host))
                continue
            for fname in sorted(ftp.nlst()):
                if not fname.endswith('.cdf'):
                    continue
                local_fname = os.path.join(root,
                                           str(year),
                                           fname)
                local_fnames.append(local_fname)
                if not os.path.isfile(local_fname):
                    dirname = os.path.dirname(local_fname)
                    if not os.path.isdir(dirname):
                        logger.info('creating path {}'.format(dirname))
                        os.makedirs(dirname)
                    logger.info('fetching missing file {}'.format(local_fname))
                    with open(local_fname, 'w') as fid:
                        ftp.retrbinary('RETR {}'.format(fname), fid.write)
                    new_fnames.append(local_fname)
    for local_fname in local_fnames:
        # convert to HDF
        local_hdf_fname = local_fname[:-3] + 'hdf'
        if not os.path.isfile(local_hdf_fname):
            logger.info('converting {} to {}'.format(local_fname,
                                                     local_hdf_fname))
            swe2hdf(local_hdf_fname,
                    local_fname)
    return new_fnames


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    root = LOCAL_ROOT

    mfi_root = os.path.join(root, 'mfi')
    mirror_mfi(mfi_root)

    swe_root = os.path.join(root, 'swe')
    mirror_swe(swe_root)
