#!/usr/bin/env python

import os
import logging
from urllib2 import urlopen, HTTPError
from datetime import datetime
from calendar import monthrange
from contextlib import closing
from gzip import GzipFile


LOGGER = logging.getLogger('mirror_carisma')


SITES = ['pols',
         'thrf',
         'osak',
         'anna']


START_YEAR = 2013

START_DATE = datetime(2013, 12, 1)

URL_TEMPLATE = 'http://data.carisma.ca/FGM/1Hz/{year}/{month:02d}/{day:02d}/{year}{month:02d}{day:02d}{site}.F01.gz'

ROOT = '/rdata/airglow/butala/data/carisma'


def mirror_carisma(sites=SITES):
    current_year = datetime.utcnow().year
    for year in range(START_YEAR, current_year + 1):
        for month in range(1, 13):
            for day in range(1, monthrange(year, month)[1] + 1):
                date = datetime(year, month, day)
                if date < START_DATE:
                    continue
                for site in sites:
                    url = URL_TEMPLATE.format(year=year, month=month, day=day, site=site.upper())
                    try:
                        with closing(urlopen(url)) as remote_fid:
                            local_path = os.path.join(ROOT, str(year), site)
                            if not os.path.isdir(local_path):
                                LOGGER.info('creating {}'.format(local_path))
                                os.makedirs(local_path)
                            local_fname_gz = os.path.join(local_path, url.split('/')[-1])
                            LOGGER.info('{} -> {}'.format(url, local_fname_gz))
                            with open(local_fname_gz, 'w') as local_fid:
                                local_fid.write(remote_fid.read())
                            local_fname = local_fname_gz[:-3]
                            LOGGER.info('{} -> {}'.format(local_fname_gz, local_fname))
                            with open(local_fname, 'w') as out_fid, GzipFile(local_fname_gz) as gz_fid:
                                out_fid.write(gz_fid.read())
                            os.remove(local_fname_gz)
                    except HTTPError:
                        LOGGER.warning('could not open {} --- skipping'.format(url))
                        continue


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    mirror_carisma()
